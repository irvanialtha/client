import Layout from '@/layout'

const gallery = {
  path: '/gallery',
  component: Layout,
  hidden: true,
  children: [
    {
      path: 'index',
      component: () => import('@/views/app/documentation/index'),
      name: 'Gallery',
      meta: { 
              title: 'Gallery',
              icon: 'gallery', 
              affix: true,
              roles: ['client']
          }
    }
  ]
}

export default gallery
