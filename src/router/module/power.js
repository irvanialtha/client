import Layout from '@/layout'

const powerusage = {
  path: '/powerusage',
  component: Layout,
  redirect: '/powerusage/floors',
  name: 'Appliance',
  meta: {
    title: 'Appliance',
    icon: 'powerusage',
    roles: ['client']
  },
  children: [
    {
      path: 'floors',
      component: () => import('@/views/app/powerusage/floor'),
      name: 'Floors',
      meta: { 
              title: 'Floors', 
              roles: ['client']
          }
    }, {
      path: 'devices',
      component: () => import('@/views/app/powerusage/device'),
      name: 'Devices',
      meta: { 
              title: 'Devices', 
              roles: ['client']
          }
    }, {
      path: 'report',
      component: () => import('@/views/app/powerusage/report'),
      name: 'Report',
      meta: { 
              title: 'Report', 
              roles: ['client']
          }
    }, {
      path: 'sumary',
      component: () => import('@/views/app/powerusage/summary'),
      name: 'Summary',
      meta: { 
              title: 'Summary', 
              roles: ['client']
          }
    }
  ]
}


export default powerusage
