/** When your routing table is too long, you can split it into small modules**/

import Layout from '@/layout'

const adminRouter = {
  path: '/admin',
  component: Layout,
  name: 'admin',
  meta: {
    title: 'Administrator',
    icon: 'user'
  },
  children: [
    {
      path: 'register',
      component: () => import('@/views/app/user/registration'),
      name: 'Register',
      meta: { 
                title: 'Register', 
                noCache: true,
                roles: ['administrator'] 
            }
    },
    {
      path: 'list user',
      component: () => import('@/views/app/user/list'),
      name: 'List User',
      meta: { 
                title   : 'List User', 
                noCache : true,
                roles: ['administrator','maintainer'] 
            }
    },
    {
      path: 'Register Client',
      component: () => import('@/views/app/user/client'),
      name: 'Register Client',
      meta: { 
                title   : 'Register Client', 
                noCache : true,
                roles: ['administrator','maintainer'] 
            }
    },
  ]
}

export default adminRouter
