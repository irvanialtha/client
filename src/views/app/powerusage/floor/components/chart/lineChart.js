import { Line } from 'vue-chartjs'

  export default {
    extends: Line,
    props: {
      chart: {
          type: Object
      },
      update:{
        type : Boolean
      }
    },
    data () {
      return {
        chartData: {
          labels: this.chart.labels,
          datasets: this.chart.datasets
        },
        options: {
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero: true
              },
              gridLines: {
                display: true
              }
            }],
            xAxes: [ {
              gridLines: {
                display: false
              }
            }]
          },
          legend: {
            display: true
          },
          responsive: true,
          maintainAspectRatio: false
        }
      }
    },
    mounted () {
      this.renderChart(this.chartData, this.options)
    },
    watch: {
      update: function(){
        this._data._chart.destroy();
        this.chartData = this.chart
        this.renderChart(this.chartData, this.options);
        this.$emit('updateChart', false)
        
      }
    },
  }