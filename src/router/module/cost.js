import Layout from '@/layout'

const cost = {
  path: '/cost',
  component: Layout,
  hidden: false,
  children: [
    {
      path: 'index',
      component: () => import('@/views/app/cost/index'),
      name: 'Cost',
      meta: { 
              title: 'Cost',
              icon: 'cost', 
              affix: true,
              roles: ['client']
          }
    }
  ]
}

export default cost
