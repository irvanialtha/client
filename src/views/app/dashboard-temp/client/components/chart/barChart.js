import {
  Bar
} from 'vue-chartjs'

function number_format(number, decimals, dec_point, thousands_sep) {
  number = (number + '').replace(',', '').replace(' ', '');
  var n = !isFinite(+number) ? 0 : +number,
      prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
      sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
      dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
      s = '',
      toFixedFix = function (n, prec) {
          var k = Math.pow(10, prec);
          return '' + Math.round(n * k) / k;
      };
  s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
  if (s[0].length > 3) {
      s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
  }
  if ((s[1] || '').length < prec) {
      s[1] = s[1] || '';
      s[1] += new Array(prec - s[1].length + 1).join('0');
  }
  return s.join(dec);
}
export default {
  extends: Bar,
  props: {
      chart: {
          type: Object
      },
      update: {
          type: Date
      }
  },
  data() {
      return {
          chartData: {
              labels: this.chart.labels,
              datasets: this.chart.datasets
          },
          options: {
              scales: {
                  yAxes: [{
                      ticks: {
                          beginAtZero: true,
                          callback: function (value) {
                              return number_format(value);
                          }
                      },
                      gridLines: {
                          display: true
                      }
                  }],
                  xAxes: [{
                      gridLines: {
                          display: false
                      }
                  }]
              },
              legend: {
                  display: true
              },
              responsive: true,
              maintainAspectRatio: false
          }
      }
  },
  mounted() {
      setTimeout(() => {
          this.renderChart(this.chartData, this.options)
      }, 200);
  },
  watch: {
      update: function () {
          this._data._chart.destroy();
          this.chartData.labels = this.chart.labels
          this.chartData.datasets = this.chart.datasets;
          this.renderChart(this.chartData, this.options);
      }
  }
}