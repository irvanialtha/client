import Layout from '@/layout'

const schedule = {
        path: '/schedule',
        component: Layout,
        hidden: true,
        children: [
          {
            path: 'index',
            component: () => import('@/views/app/documentation/index'),
            name: 'Cost',
            meta: { 
                    title: 'Schedule',
                    icon: 'documentation', 
                    affix: true,
                    roles: ['client']
                }
          }
        ]
    }


export default schedule
