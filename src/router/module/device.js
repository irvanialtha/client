import Layout from '@/layout'

const devices = {
        path: '/devices',
        component: Layout,
        children: [
          {
            path: 'index',
            component: () => import('@/views/app/device/index'),
            name: 'Devices',
            meta: { 
                    title: 'Devices',
                    icon: 'devices', 
                    affix: true,
                    roles: ['client']
                  }
          }
        ]
    }


export default devices
