import Layout from '@/layout'

const settings = {
  path: '/settings',
  component: Layout,
  hidden: true,
  children: [ {
      path: 'index',
      component: () => import('@/views/app/documentation/index'),
      name: 'Settings',
      meta: { 
              title: 'Settings',
              icon: 'documentation', 
              affix: true,
              roles: ['administrator','maintainer','client']
          }
    }
  ]
}

export default settings
