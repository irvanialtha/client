import Layout from '@/layout'

const cctv = {
  path: '/cctv',
  component: Layout,
  hidden: true,
  children: [
    {
      path: 'index',
      component: () => import('@/views/app/documentation/index'),
      name: 'CCTV',
      meta: { 
              title: 'CCTV',
              icon: 'cctv', 
              affix: true,
              roles: ['client']
          }
    }
  ]
}

export default cctv
