import Layout from '@/layout'

const histroy = {
  path: '/histroy',
  component: Layout,
  hidden: true,
  children: [
    {
      path: 'index',
      component: () => import('@/views/app/documentation/index'),
      name: 'History',
      meta: { 
              title: 'History',
              icon: 'history', 
              affix: true,
              roles: ['client']
          }
    }
  ]
}

export default histroy
