// import { HorizontalBar  } from 'vue-chartjs'

//   export default {
//     extends: HorizontalBar ,
//     data() {
//       return {
//         chartData: {
//           labels: ["2015-01", "2015-02", "2015-03", "2015-04", "2015-05", "2015-06", "2015-07", "2015-08", "2015-09",
//             "2015-10", "2015-11", "2015-12"
//           ],
//           datasets: [{
//             label: 'Bar Chart',
//             borderWidth: 1,
//             backgroundColor: [
//               'rgba(255, 99, 132, 0.2)',
//               'rgba(54, 162, 235, 0.2)',
//               'rgba(255, 206, 86, 0.2)',
//               'rgba(75, 192, 192, 0.2)',
//               'rgba(153, 102, 255, 0.2)',
//               'rgba(255, 159, 64, 0.2)',
//               'rgba(255, 99, 132, 0.2)',
//               'rgba(54, 162, 235, 0.2)',
//               'rgba(255, 206, 86, 0.2)',
//               'rgba(75, 192, 192, 0.2)',
//               'rgba(153, 102, 255, 0.2)',
//               'rgba(255, 159, 64, 0.2)'
//             ],
//             borderColor: [
//               'rgba(255,99,132,1)',
//               'rgba(54, 162, 235, 1)',
//               'rgba(255, 206, 86, 1)',
//               'rgba(75, 192, 192, 1)',
//               'rgba(153, 102, 255, 1)',
//               'rgba(255, 159, 64, 1)',
//               'rgba(255,99,132,1)',
//               'rgba(54, 162, 235, 1)',
//               'rgba(255, 206, 86, 1)',
//               'rgba(75, 192, 192, 1)',
//               'rgba(153, 102, 255, 1)',
//               'rgba(255, 159, 64, 1)'
//             ],
//             pointBorderColor: '#2554FF',
//             data: [12, 19, 3, 5, 2, 3, 20, 3, 5, 6, 2, 1]
//           }]
//         },
//         options: {
//           scales: {
//             yAxes: [{
//               ticks: {
//                 beginAtZero: true
//               },
//               gridLines: {
//                 display: true
//               }
//             }],
//             xAxes: [{
//               gridLines: {
//                 display: false
//               }
//             }]
//           },
//           legend: {
//             display: true,
//             position : 'right'
//           },
//           responsive: true,
//           maintainAspectRatio: false
//         }
//       }
//     },
//     mounted() {
//       this.renderChart(this.chartData, this.options)
//     }
//   }

import {
  HorizontalBar
} from 'vue-chartjs'

function number_format(number, decimals, dec_point, thousands_sep) {
  number = (number + '').replace(',', '').replace(' ', '');
  var n = !isFinite(+number) ? 0 : +number,
      prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
      sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
      dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
      s = '',
      toFixedFix = function (n, prec) {
          var k = Math.pow(10, prec);
          return '' + Math.round(n * k) / k;
      };
  s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
  if (s[0].length > 3) {
      s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
  }
  if ((s[1] || '').length < prec) {
      s[1] = s[1] || '';
      s[1] += new Array(prec - s[1].length + 1).join('0');
  }
  return s.join(dec);
}
export default {
  extends: HorizontalBar,
  props: {
      chart: {
          type: Object
      },
      update: {
          type: Date
      }
  },
  data() {
      return {
          chartData: {
              labels: this.chart.labels,
              datasets: this.chart.datasets
          },
          options: {
              scales: {
                  yAxes: [{
                      ticks: {
                          beginAtZero: true
                      },
                      gridLines: {
                          display: true
                      }
                  }],
                  xAxes: [{
                      ticks:{
                        callback: function (value) {
                          return number_format(value);
                        }
                      },
                      gridLines: {
                          display: false
                      }
                  }]
              },
              legend: {
                  display: true
              },
              responsive: true,
              maintainAspectRatio: false
          }
      }
  },
  mounted() {
      setTimeout(() => {
          this.renderChart(this.chartData, this.options)
      }, 200);
  },
  watch: {
      update: function () {
          this._data._chart.destroy();
          this.chartData.labels = this.chart.labels
          this.chartData.datasets = this.chart.datasets;
          this.renderChart(this.chartData, this.options);
      }
  }
}