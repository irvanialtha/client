import Vue from 'vue'
import Cookies from 'js-cookie'
import Element from 'element-ui'

import App from './App'
import store from './store'
import router from './router'
import enLang from 'element-ui/lib/locale/lang/en'
import PerfectScrollbar from 'vue2-perfect-scrollbar'

import './icons'
import './permission' 
import './utils/error-log' 
import '@/styles/index.scss'
import 'normalize.css/normalize.css'
import './styles/element-variables.scss'
import 'vue2-perfect-scrollbar/dist/vue2-perfect-scrollbar.css'

// register global utility filters
import * as filters from './filters'
Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})

Vue.use(Element, {
  size: Cookies.get('size') || 'medium',
  locale : enLang
})
Vue.use(PerfectScrollbar)
Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})