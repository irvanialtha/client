import request from '@/utils/request'

export function register(data) {
  return request({
    url: 'client/register',
    method: 'post',
    data
  })
}

export function get() {
  return request({
    url: 'client/get',
    method: 'get'
  })
}

export function role() {
  return request({
    url: 'client/role',
    method: 'get'
  })
}

export function getbyuser() {
  return request({
    url: 'client/getbyuser',
    method: 'get'
  })
}

export function getfloorclient(data) {
  return request({
    url: 'client/getfloorclient',
    method: 'get',
    params: data
  })
}

export function getfloordevice(data) {
  return request({
    url: 'client/getfloordevice',
    method: 'get',
    params: data
  })
}

export function createdevice(data) {
  return request({
    url: 'client/createdevice',
    method: 'post',
    data
  })
}

export function gettypedevice() {
  return request({
    url: 'client/typedevice',
    method: 'get'
  })
}

export function getclientdevice(data) {
  return request({
    url: 'client/device',
    method: 'get',
    params: data
  })
}

export function getappliancefloor(data) {
  return request({
    url: 'client/getappliancefloor',
    method: 'get',
    params: data
  })
}

export function getalldevice() {
  return request({
    url: 'client/getalldevice',
    method: 'get'
  })
}

export function getchangeincost(data) {
  return request({
    url: 'client/getchangeincost',
    method: 'get',
    params: data
  })
}

export function getusageperdevice(data) {
  return request({
    url: 'client/getusageperdevice',
    method: 'get',
    params: data
  })
}

export function getusageperfloor(data) {
  return request({
    url: 'client/getusageperfloor',
    method: 'get',
    params: data
  })
}

export function downloadreportdevice(data) {
  return request({
    url: 'client/downloadreportdevice',
    method: 'get',
    params: data,
    responseType: 'blob'
  })
}

export function downloadreportdevice2(data) {
  return request({
    url: 'client/downloadreportdevice2',
    method: 'get',
    params: data,
    responseType: 'blob'
  })
}

export function getdevicestatus(data) {
  return request({
    url: 'client/getdevicestatus',
    method: 'post',
    data
  })
}
